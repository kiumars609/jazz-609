import React, { useRef } from 'react';
import './style.css'
import { impSounds } from './impSounds';
import Img from './drum.png'

export default function Jazz() {
  const inputRef = useRef();
  const playDrum = () => {
    inputRef.current.focus();
  }
  const handleKeyPress = (e) => {
    impSounds(e)
  }
  const guidList = [
    { btn: 'Y', title: 'Crash' },
    { btn: 'U', title: 'Ride' },
    { btn: 'R', title: 'Hi-hat(closed)' },
    { btn: 'E', title: 'Hi-hat(open)' },
    { btn: 'C', title: 'Hi-hat(foot)' },
    { btn: 'G', title: 'High tom-tom' },
    { btn: 'H', title: 'Low tom-tom' },
    { btn: 'J', title: 'Floor tom' },
    { btn: 'S', title: 'Snare drum' },
    { btn: 'D', title: 'Snare drum (cross stick)' },
    { btn: 'X', title: 'Bass drum' },
  ]
  const mapGuid = guidList.map((data, index) => {
    return (
      <>
        <div key={index} className="col-md-4 mb-2">
          <button className='btn btn-light col-md-6'>{data.btn}</button>
        </div>
        <div className="col-md-8 text-white">
          {data.title}
        </div>
      </>
    )
  })

  return (
    <div className="row col-md-12 mx-auto">
      <div className="col-md-9 text-center position-relative">
        <img src={Img} className='mt-5 col-md-8' />
      </div>
      <div className="col-md-3 menu-options">
        <p>
          Key Mapping
        </p>
        <button className='btn btn-success col-md-12 mb-3' onClick={() => playDrum()}>Start</button>
        <div className="col-md-12 row mx-auto">
          {mapGuid}
        </div>
      </div>
      <input type="text" className='none-inp col-md-1' onKeyPress={(e) => handleKeyPress(e)} ref={inputRef} />
    </div>
  )
}
