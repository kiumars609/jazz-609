import crashSound from './sound/crash.mp3';
import rideSound from './sound/ride.mp3';
import hiHatClosedSound from './sound/hiHatClosed.mp3';
import hiHatOpenSound from './sound/hiHatOpen.mp3';
import hiHatFootSound from './sound/hiHatFoot.mp3';
import highTomTomSound from './sound/hightTomTom.mp3';
import lowTomTomSound from './sound/lowTomTom.mp3';
import floorTomSound from './sound/floorTom.mp3';
import snareDrumSound from './sound/snareDrum.mp3';
import snareDrumCrossStickSound from './sound/snareDrumCrossStick.mp3';
import bassDrumSound from './sound/bassDrum.mp3';

export function impSounds(e) {
    if (e.key === "y" || e.key === "Y") {
        var Crash = new Audio(crashSound);
        Crash.play();
    }
    else if (e.key === "u" || e.key === "U") {
        var Ride = new Audio(rideSound);
        Ride.play();
    }

    else if (e.key === "r" || e.key === "R") {
        var HiHatClosed = new Audio(hiHatClosedSound);
        HiHatClosed.play();
    }

    else if (e.key === "e" || e.key === "E") {
        var HiHatOpen = new Audio(hiHatOpenSound);
        HiHatOpen.play();
    }

    else if (e.key === "c" || e.key === "C") {
        var HiHatFoot = new Audio(hiHatFootSound);
        HiHatFoot.play();
    }

    else if (e.key === "g" || e.key === "G") {
        var HighTomTom = new Audio(highTomTomSound);
        HighTomTom.play();
    }

    else if (e.key === "h" || e.key === "H") {
        var LowTomTom = new Audio(lowTomTomSound);
        LowTomTom.play();
    }

    else if (e.key === "j" || e.key === "J") {
        var FloorTom = new Audio(floorTomSound);
        FloorTom.play();
    }

    else if (e.key === "s" || e.key === "S") {
        var SnareDrum = new Audio(snareDrumSound);
        SnareDrum.play();
    }

    else if (e.key === "d" || e.key === "D") {
        var SnareDrumCrossStick = new Audio(snareDrumCrossStickSound);
        SnareDrumCrossStick.play();
    }

    else if (e.key === "x" || e.key === "X") {
        var BassDrum = new Audio(bassDrumSound);
        BassDrum.play();
    }
}
